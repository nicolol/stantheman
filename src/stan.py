# %%
import requests
import time
import os
import sqlite3
from threading import Thread, Lock
from datetime import datetime
import numpy as np
import pandas as pd
from requests.exceptions import ConnectionError


# set the starting point for accessing resources
base_dir = os.path.dirname(__file__)

# url to ping
url = "https://www.init7.net"


class StanTheMan():



    def __init__(self):
        self.db_location = base_dir + '/../pings.db'
        self.db_lock = Lock()


    def ping(self):

        try:
            # start timer
            start_time = time.time()
            response = requests.get(url)

            # end timer
            end_time = time.time()
            delta = end_time - start_time
            status = response.status_code

        except ConnectionError as err:
            delta = np.nan
            status = 404

        return status, delta


    def task(self):

        date = datetime.now()
        status, delta = self.ping()
        row = pd.Series({'date': date, 'status': status, 'response_time': delta})

        self.db_lock.acquire()
        print("{}, status:{}, time to response: {}".format(date, status, delta))
        conn = sqlite3.connect(self.db_location)
        pd.DataFrame().append(row, ignore_index=True).\
            to_sql('pings', conn, if_exists='append')

        self.db_lock.release()


    def loop(self):

        while True:

            thread = Thread(target=self.task)
            thread.start()

            time.sleep(1.0)



# %%

if __name__=="__main__":

    s = StanTheMan()
    s.loop()
