#!/bin/sh

ps auxw | grep stan.py | grep -v grep > /dev/null

if [ $? != 0 ]
then
    python ~/stantheman/src/stan.py >> ~/.logs/stan.log
fi
